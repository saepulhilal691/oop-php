<?php

class Ape extends Animal {
    public $legs = 2;
    public $voice = "Auuoo";
    public function yell()
    {
        echo $this->voice;
    }
}