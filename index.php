<?php
require('animal.php');
require('ape.php');
require('frog.php');


$sheep = new Animal("shaun");
echo "Name : " . $sheep->name; // "shaun"
echo "<br>";
echo "legs : " . $sheep->legs; // 4
echo "<br>";
echo "cold blooded : " . $sheep->cold_blooded; // "no"
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name; 
echo "<br>";
echo "legs : " . $sungokong->legs;
echo "<br>";
echo "cold blooded : " . $sungokong->cold_blooded;
echo "<br>";
echo "Yell : "; 
$sungokong->yell(); 
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo $kodok->name;
echo "<br>";
echo "legs : " . $kodok->legs;
echo "<br>";
echo "cold blooded : " . $sungokong->cold_blooded;
echo "<br>";
echo "Jump : "; 
$kodok->jump();